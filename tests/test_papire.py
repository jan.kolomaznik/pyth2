import pytest

from papire import *


def test_papire_put_mark():
    # Arrange
    papire = Papire({Coor(0, 0): 'X'})
    # Act
    result = papire.put_mark(Coor(0, 1), 'O')
    # Assert
    assert result.turns == {Coor(0, 0): 'X', Coor(0, 1): 'O'}
    
def test_papire_put_mark_to_occupired_position():
    # Arrange
    papire = Papire({Coor(0, 0): 'X'})
    # Act and Assert
    with pytest.raises(ValueError) as e:
        result = papire.put_mark(Coor(0, 0), 'O')