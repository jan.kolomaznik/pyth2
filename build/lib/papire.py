from typing import Dict, NamedTuple


class Coor(NamedTuple):
    x: int
    y: int


class Papire:

    def __init__(self, turns: Dict[Coor, str] = {}) -> None:
        self.turns = turns

    def __str__(self) -> str:
        return f"{self.turns}"

    def put_mark(self, coor: Coor, mark: str) -> "Papire":
        """
        Places a symbol on the virtual paper in the game Piskvorky at the specified
        position.
        
        >>> papire = Papire({Coor(0, 0): 'X'})
        >>> papire.put_mark(Coor(0, 1), 'O').turns
        {Coor(x=0, y=0): 'X', Coor(x=0, y=1): 'O'}

        Args:
            coor (Coor): The coordinates where the symbol is to be placed.
            mark (str): The symbol to be placed at the specified coordinates.

        Returns:
            Papire: A new instance of the Papire class with the symbol placed.

        Raises:
            ValueError: If the specified position is already occupied.
        """
        if coor in self.turns:
            raise ValueError("The specified position is already occupied.")

        new_turns = self.turns.copy()
        new_turns[coor] = mark

        return Papire(new_turns)

if __name__ == '__main__':
    import doctest
    doctest.testmod()