
import re
import glob
import sys

if __name__ == '__main__':
    # print('sys.argv:', sys.argv)
    file_pattern = sys.argv[1] if len(sys.argv) > 1 else '*'
    regex = sys.argv[2] if len(sys.argv) > 2 else '.*'
    
    print(f'file_pattern: {file_pattern}, regex: {regex}')
    for file in glob.iglob(file_pattern, recursive=True):
        try:
            with open(file) as f:
                print(file)
                for i, line in enumerate(f):
                    if m := re.search(regex, line):
                        print(f"{i:>5}: {line}", m.pos)
        except:
            print(f'Error reading {file}')            