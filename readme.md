Python - pokročilé programování
=============================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Kurz je určen vývojářům, kteří ovládají základy programování v jazyce Python a chtěji prohloubit své znalosti o pokročilejší techniky jako zpracování textu, tvorbu vícevláknových a paralelních aplikací, případně i jejich ladění.

Osnova kurzu:
------------

- **Seznámení s jazykem Python**
- **[Základy programování v Python](00-python.ipynb)**
    - Tak prvně proč Python
    - Charakteristika
    - K čemu se python hodí?
    - Kdo python používá?
- **[Pracovní prostředí](01-python.developement.ipynb)**
    - Instalace
    - Možnosti vývoje
    - Příprava na vývoj
    - IDE
- **[Základy jazyka](02-python.basic.ipynb)**
    - Komentáře
    - Vystup konsole (`print()`)
    - Proměnné
    - Vstup z konsole (`input()`)
    - Pravidla pro pojmenování proměnných
    - Operátory přiřazení
    - Rezervovaná slova
    - Konce řádků a středníky
    - Import funkci, objeku, modulu
- **Datové typy**
- **[Datové typy](03-python.type.ipynb)**
    - Základní datové typy
    - Prázdná hodnota
    - Logické hodnoty (bool)
    - Číselné datové typy
    - Řetězce (str)
    - Formátování řetězce
    - Datum a čas
    - Konverze datových typů
  * Python tyting
  * Binární operace
- **[Čísla](04-python.type.numbers.ipynb)**
    - Number
    - Decimal
    - Fraction
- **[Řetězce: Formátování](05-python.type.str.ipynb)**
    - Převod parametru na řetězec
    - Zorvnání formátorvání
- **[Regulární výrazy](06-python.type.regex.ipynb)**
    - Modul `re`
    - Základní použití:
    - Kompilace regulárních výrazů
    - Match objekt
- *Strukturované typy*
- **Základní konstrukce jazyka**
- **[Základní konstrukce jazyka](07-python.construction.ipynb)**
    - Podmínka (`if`)
    - Smyčka (`while`)
    - Smyčka (`for`)
- **[Ternární operátor](08-python.construction.if.ipynb)**
- **[For při generování seznamů](09-python.construction.for.ipynb)**
- **Funkce**
    - **[Funkce](10-python.function.ipynb)**
    - **[Lambdas](11-python.function.lambdas.ipynb)**
    - **[Dekorátory](12-python.function.decorator.ipynb)**
    - **[Functools](13-python.function.functools.ipynb)**
    - **[nerátory](14-python.function.genarators.ipynb)**
    - **[Itertools](15-python.function.itertools.ipynb)**

- **Přehled základních vlastností jazyka**
  - Proměnné a reference
  - Standardní datové typy
  - Řídící struktury
- **Organizace projektu**
  - Funkce
  - Třídy a metody
  - Moduly a balíčky
  - Distribuce software
- **Funkce a metody**
  - Poziční a pojmenované parametry
  - Pokročilé zpracování argumentů
  - Vnořené funkce a funkcionální prvky
  - Globální, lokální a vázané proměnné
- **Objektový model**
  - Objektově orientovaný návrh
  - Datové typy a operace
  - Speciální metody
  - Deskriptory
  - Dědičnost a polymorfie
  - Vícenásobná dědičnost a mixins
  - Základní návrhové vzory
- **Generátory a iterátory**
  - Sekvenční datové typy
  - Generované sekvence
  - Čtení textových souborů
- **Ukládání a zpracování dat**
  - Ukládání objektů
  - Datové formáty
  - Přístup k databázi
- **Online komunikace**
  - Webový klient a server
  - Práce s API
- **Ladění a logování**
  - Ladění pomocí výpisů
  - Standardní logovací knihovna
  - Debuggery
- **Testování**
  - Testování knihoven a aplikací
  - Organizace testů
- **Paralelní zpracování**
  - Vlákna a procesy
  - Komunikace a synchronizace
  - Výkonnostní omezení Pythonu
- **Pokročilá témata**
  - Generování kódu
  - Monkey patching


- **Základy jazyka**


- **Struktury (generátory a iterátory)**
    * data-class
    * named-tuple


- **Třídy a objekty**
    - **[Magické metody](16-python.object.magic.ipynb)**
    - **[First-Class Objects](17-python.object.first-class.ipynb)**
    - **[Deskriptory](18-python.object.descriptor.ipynb)**
    - **[Konstruktor](19-python.object.constuctor.ipynb)**
    - **[Metatřídy](20-python.object.metaclass.ipynb)**
    * Vícenásobná dědičnost
- **Paralelizace**
    - **[Úvod multithreading vs. multiprocessing](21-python.parallelization.ipynb)**
    - **[Multithreading](22-python.parallelization.multihreading.ipynb)**
    - **[Multiprocessing](23-python.parallelization.multiprocessing.ipynb)**
    * Concurrent.futures
- **Práce s daty**
    * pickle
    * shelve
    * json
    * databáze
- **Tvorba aplikace**
    * Vytvoření a organizace velkého projektu
    * Tvorba vlastního modulu a jeho distribuce
    * logování
- **Testování**
    - **[Testování](24-python.test.unittest.ipynb)**
 